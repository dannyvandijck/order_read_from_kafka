package com.essers.orderdomain.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.essers.orderdomain.service.OrderService;

@RestController
public class OrderController {
	@Autowired
	private OrderService orderService;

	@GetMapping("/orders")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String getOrders() {
		return orderService.getAllOrders().toString();
	}

	@GetMapping("/ordersamount")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String getOrdersAmount() {
		return String.valueOf(orderService.getAllOrders().size());
	}

}
