package com.essers.orderdomain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.essers.orderdomain.model.Order;

public interface OrderDomainRepository extends JpaRepository<Order, Long> {

}
