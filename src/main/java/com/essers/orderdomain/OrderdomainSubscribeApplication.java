package com.essers.orderdomain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderdomainSubscribeApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderdomainSubscribeApplication.class, args);
	}
}
