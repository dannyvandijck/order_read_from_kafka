package com.essers.orderdomain.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface OrderTopicStreams {

	String READ_NEW_ORDER_FROM_STREAM = "orderTopic-new";

	@Input(READ_NEW_ORDER_FROM_STREAM)
	SubscribableChannel readUpdateForOrderFromOrderTopic();

}
