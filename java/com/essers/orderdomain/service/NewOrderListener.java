package com.essers.orderdomain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.essers.orderdomain.model.Order;
import com.essers.orderdomain.stream.OrderTopicStreams;

@Component
public class NewOrderListener {

	@Autowired
	private OrderService orderService;

	@StreamListener(OrderTopicStreams.READ_NEW_ORDER_FROM_STREAM)
	public void readOrderFromTopic(@Payload Order order) {
		orderService.save(order);
	}
}
