package com.essers.orderdomain.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.essers.orderdomain.model.Order;
import com.essers.orderdomain.repository.OrderDomainRepository;

@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderDomainRepository orderDomainRepository;

	public List<Order> getAllOrders() {
		return orderDomainRepository.findAll();
	}

	public Order save(Order order) {

		Optional<Order> existingOrder = orderDomainRepository.findById(order.getId());

		if (!existingOrder.isPresent()) {
			order.setOrderVersion(1);
		} else {
			order.setOrderVersion(existingOrder.get().getOrderVersion());
		}

		return orderDomainRepository.save(order);
	}
}
