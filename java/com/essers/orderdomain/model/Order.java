package com.essers.orderdomain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "WEB_ORDER")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "ORDER_TOTAL")
	private double orderTotal;
	@Column(name = "ORDER_CUSTOMER_ID")
	private long orderCustomerId;
	@Column(name = "ORDER_CREATION_TIMESTAMP")
	@javax.persistence.Temporal(TemporalType.TIMESTAMP)
	private Date orderCreationTimestamp;
	// when updating it checks the version, if it already was updated --> OptimisticLockException
	@Version
	@JsonIgnore
	@Column(name = "OPT_LOCK_VERSION", columnDefinition = "BIGINT DEFAULT 0", nullable = false)
	private Integer orderVersion;

}
