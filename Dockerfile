FROM openjdk:8-jre-slim
MAINTAINER Danny Van Dijck <danny.vandijck@hotmail.com>
ADD target/orderread.jar orderread.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/orderread.jar"]
EXPOSE 2345
